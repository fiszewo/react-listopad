import React from 'react'
import { TodosLists } from './app-todos'
import { Users } from './users/users.container'

// https://reacttraining.com/react-router/
import {
  HashRouter as Router,
  Route,
  Link,
  NavLink,
  Switch,
  Redirect
} from 'react-router-dom'
const Tabs = props => props.children.filter(
  elem => elem.key == props.tab
)

export class App extends React.Component {

  render() {
    return <div>
      <div className="nav nav-tabs">
        <NavLink activeClassName="active" className="nav-link" to="/todos">Todos</NavLink>
        <NavLink activeClassName="active" className="nav-link" to="/users">Users</NavLink>
      </div>
      <Route path="/" exact render={props =>
        <Redirect to="/todos" />
      } />
      <div className="row">
        <div class="col">
          <Route path="/todos" exact component={TodosLists} />
          <Route path="/users" component={Users} />
        </div>
      </div>
    </div>
  }
}
// App.defaultProps
