import React from 'react'

export const TodosList = ({
  title, style: myStyle, todos, onToggle, onRemove
}) => <div>
    <h3 style={myStyle}>{title}</h3>
    <div className="list-group">
      {todos.map((todo, index) =>
        <div key={todo.id} className="list-group-item">

          <input type="checkbox" checked={todo.completed}
            onChange={e => onToggle(todo, e)}
          /> {todo.title}

          {onRemove && <span className="close" onClick={e => onRemove(todo.id)}>&times;</span>}
        </div>
      )}
    </div>
  </div>

TodosList.defaultProps = {
  onToggle: () => { }
}